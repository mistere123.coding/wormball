
--all files are in the minigame_manager folder

dofile(core.get_modpath("wormball") .. "/minigame_manager/on_load.lua")

dofile(core.get_modpath("wormball") .. "/minigame_manager/on_start.lua")

dofile(core.get_modpath("wormball") .. "/minigame_manager/on_time_tick.lua")

dofile(core.get_modpath("wormball") .. "/minigame_manager/globalstep.lua")

dofile(core.get_modpath("wormball") .. "/minigame_manager/on_eliminate.lua")

--dofile(core.get_modpath("wormball") .. "/minigame_manager/on_timeout.lua")

dofile(core.get_modpath("wormball") .. "/minigame_manager/on_quit.lua")

dofile(core.get_modpath("wormball") .. "/minigame_manager/on_celebration.lua")
