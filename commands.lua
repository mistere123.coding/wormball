local cmd = ChatCmdBuilder.register("wormball", {
    description = [[
        (/help wormball)
        - clrleaderboard <arena_name> -- clears the highscores of a wormball arena
        - leaderboard <arena_name> <player_name> -- shows the leaderboard of a wormball arena to a player
        - version -- view womball version
    ]],
    params = "clearleaderboard <arena_name> | leaderboard <arena_name> <player_name> | version",
    privs = { wormball_admin = true },
})

--clear highscores
cmd:sub("clrleaderboard :arena", function(name, arena)
    local success, msg = wormball.leaderboard.clear_highscores(arena)
    if success == true then 
        return "[!] Wormball arena " .. arena .. " Highscores Cleared!"
    else
        return "[!] Highscore Clear Attempt Failed! Error: ".. msg
    end
end)

cmd:sub("leaderboard :arena_name :p_name:username", function(name, arena_name, p_name)
    local arena = arena_lib.get_arena_by_name('wormball', arena_name )
    if not wormball.show_singleplayer_leaderboard( arena , p_name ) then
        return "Invalid usage: please specify a valid wormball arena name to view its leaderboard."
    end
end)

cmd:sub("version", function(name)
    return true, "Wormball Version: "..wormball.version
end)




core.register_chatcommand("wormball_leaderboard", {
	params = "<arena_name>",
	description = [[
        View the singleplayer leaderboard of a wormball arena
        - arena_name -- must be a valid wormball arena name
    ]],
	privs = {},
	func = function( name , arena_name)
        if not wormball.show_singleplayer_leaderboard( arena_name , name ) then
            return "Invalid usage: please specify a valid wormball arena name to view its leaderboard."
        end
	end,
})



core.register_chatcommand("wormball_multiscores", {
	params = "<arena_name>",
	description = [[
        View the scores of players of the last game played in a wormball arena
        - arena_name -- must be a valid wormball arena name
    ]],
	privs = {},
	func = function( name , arena_name)
        if not arena_lib.get_arena_by_name('wormball', arena_name ) then return "Invalid usage: please specify a valid wormball arena name to view its leaderboard." end
        local arena_id, arena = arena_lib.get_arena_by_name('wormball', arena_name )
        if not arena.multi_scores or arena.multiscores == {} then return "no scores found!" end
        if not wormball.show_multi_scores( arena , name , arena.multi_scores ) then
            return "Invalid usage: please specify a valid wormball arena name to view its leaderboard."
        end
        return "error"
	end,
})


