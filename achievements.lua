local S = core.get_translator("wormball")

achvmt_lib.register_mod("wormball", {
    name = S("Wormball"),
    icon = "magiccompass_wormball.png",
})




function wormball.award(p_name, ach_name)
    if not achvmt_lib.has_achievement(p_name, ach_name) then
        achvmt_lib.award(p_name, ach_name)
    end
end

-- wormball achievements

-- length based achievements

achvmt_lib.register_achievement("wormball:hatchling", {
    title = S("Hatchling"),
    description = S("Get more than 10 points"),
    additional_info = S("First taste of the world!"),
    image = "wormball_ach_hatchling.png",
    tier = "Bronze",
})

achvmt_lib.register_achievement("wormball:tail_shaker", {
    title = S("Tail Shaker"),
    description = S("Get more than 50 points"),
    additional_info = S("Your length is starting to demand attention!"),
    image = "wormball_ach_tail_shaker.png",
    tier = "Silver",
})

-- hookworthy
achvmt_lib.register_achievement("wormball:hookworthy", {
    title = S("Hook-worthy"),
    description = S("Get more than 100 points"),
    additional_info = S("A worm with undeniable appeal!"),
    image = "wormball_ach_hook_worthy.png",
    tier = "Gold",
})



-- other achievements

-- cannibal worm

achvmt_lib.register_achievement("wormball:cannibal_worm", {
    title = S("Cannibal Worm"),
    description = S("Eat your own color. Yuck!"),
    image = "wormball_ach_cannibal_worm.png",
    tier = "Bronze",
    hidden = true,
})

achvmt_lib.register_achievement("wormball:global_worming", {
    title = S("Global Worming"),
    description = S("Win with more than 10 pts in multiplayer"),
    additional_info = S("4+ players required"),
    image = "wormball_ach_global_worming.png",
    tier = "Silver",
    hidden = false,
})

achvmt_lib.register_achievement("wormball:coil_king", {
    title = S("Coil King"),
    description = S("Beat the High Score."),
    additional_info = S("You've reached the top of the Wormball hierarchy."),
    image = "wormball_ach_coil_king.png",
    tier = "Gold",
    hidden = false,
})
